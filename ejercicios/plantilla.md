# Esta es una plantilla para practicar en clase


## Informe de seguridad para SISTEMX

Tras la auditoría, se ha detectado que el sistema es muy inseguro porque tiene graves fallos de seguridad y vulnerabilidades. 

Las categorías de OWASP que se han analizado son:

- Categoria A
Las contraseñas se encuentran visibles en un fichero de texto plano.

- Categoria B
Sin shell.
Fichero optware.oleg. SW y versión visible.

- Categoría C
Hay más de un root.
Varios usuarios con el mismo UID.
Carpeta Home fuera de Home.

Las recomendaciones de seguridad son las siguientes:

- Recomendación 1: Utilizar contraseñas complejas que sean fáciles de recordar o, al menos, cifrar el fichero que las contiene.
- Recomendación 2: Sólo debe haber un usuario root.
- Recomendación 3: Tener un UID único para cada usuario.
- Recomendación 4: Tener las carpetas del sistema y usuarios en su lugar correspondiente sin que sean fácilmente accesibles.



